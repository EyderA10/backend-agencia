<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220409172706 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE viajeros (id INT AUTO_INCREMENT NOT NULL, cedula VARCHAR(20) NOT NULL, nombre VARCHAR(30) NOT NULL, fecha_nacimiento DATE NOT NULL, num_telf VARCHAR(30) NOT NULL, UNIQUE INDEX UNIQ_71FD11BA7BF39BE0 (cedula), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viajes (id INT AUTO_INCREMENT NOT NULL, codigo_viaje INT NOT NULL, numero_plazas INT NOT NULL, destino VARCHAR(50) NOT NULL, lugar_origen VARCHAR(50) NOT NULL, precio INT NOT NULL, UNIQUE INDEX UNIQ_EFC73BB74E92BC1B (codigo_viaje), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE viaje_viajero (id INT AUTO_INCREMENT NOT NULL, viaje_id INT NOT NULL, viajero_id INT NOT NULL, INDEX IDX_B361D7ED94E1E648 (viaje_id), INDEX IDX_B361D7EDFAF4CFE9 (viajero_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE viaje_viajero DROP FOREIGN KEY FK_B361D7EDFAF4CFE9');
        $this->addSql('ALTER TABLE viaje_viajero DROP FOREIGN KEY FK_B361D7ED94E1E648');
        $this->addSql('DROP TABLE viajeros');
        $this->addSql('DROP TABLE viajes');
        $this->addSql('DROP TABLE viaje_viajero');
    }
}
