<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Creacion de una clase abstracta para poder construir el form y deshabilitar el token csrf
 * y reusar el metodo respond
 */
abstract class AbstractApiController extends AbstractController
{
    public function buildForm(string $type, $data = null, array $options = []): FormInterface
    {

        $options = array_merge($options, [
            'csrf_protection' => false
        ]);

        return $this->container->get('form.factory')->create($type, $data, $options);
    }

    public function respond($data, int $statusCode = Response::HTTP_OK): JsonResponse
    {
        return new JsonResponse($data, $statusCode);
    }

    /**
     * Mapeando los errores para enviarlos por json
     * 
     * @param FormInterface $form
     */
    public function thereErrors(FormInterface $form): array
    {
        /**
         * @var array $errors
         */
        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            if ($error->getOrigin()) {
                $errors[$error->getOrigin()->getName()][] = $error->getMessage();
            }
        }
        return $errors;
    }
}
