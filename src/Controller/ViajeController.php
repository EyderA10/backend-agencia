<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;
use App\Entity\Viaje;
use App\Entity\Viajero;
use App\Form\Type\ViajeType;
use App\Repository\ViajeRepository;
use App\Repository\ViajeroRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class ViajeController extends AbstractApiController
{

    /**
     * @var array $data
     */
    private $data;

    public function __construct()
    {
        $this->data = [];
    }

    /**
     * @param Request $request
     * @param ViajeRepository $viaje
     * @param ViajeroRepository $viajero
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    public function assignViajeAViajero(Request $request, ViajeRepository $viaje, ViajeroRepository $viajero, ManagerRegistry $doctrine): JsonResponse
    {
        $content = json_decode($request->getContent());

        /**
         * @var int $viaje_id
         * @var int $viajero_id
         */
        $viaje_id = $content->viaje_id;
        $viajero_id = $content->viajero_id;

        if ($viaje_id === "" || $viaje_id === "") {
            $this->data = [
                'status' => false,
                'message' => 'el campo viajero_id y viaje_id son necesarios!'
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        /**
         * @var Viaje|null $trip
         */
        $trip = $viaje->createQueryBuilder('t')
            ->select('t')
            ->where('t.id = :id')
            ->setParameter('id', $viaje_id)
            ->getQuery()
            ->getArrayResult();

        /**
         * @var Viajero|null $viajer
         */
        $viajer = $viajero->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajero_id)
            ->getQuery()
            ->getArrayResult();

        // Si existe el viaje y el viajero
        if (!$trip || !$viajer) {
            $this->data = [
                'status' => false,
                'message' => 'No se encontraron viajes o viajeros'
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        //Insercion a la tabla viaje_viajero con QueryBuilder\DBL
        // con el $doctrine (ManagerRegistry) puedo obtener la coneccion a la db
        $qb = new QueryBuilder($doctrine->getConnection());
        $qb->insert('viaje_viajero')
            ->values([
                'viaje_id' => '?',
                'viajero_id' => '?',
            ])
            ->setParameters([
                $viaje_id,
                $viajero_id,
            ]);
        $qb->executeStatement();

        //devuelvo los viajeros que se le asigo el viaje y 
        //para poder mostrarlo y actualizar el estado en el front
        $qb2 = new QueryBuilder($doctrine->getConnection());
        $qb2->select('*')
            ->from('viaje_viajero', 'vvj')
            ->innerJoin('vvj', 'viajeros', 'v', 'vvj.viajero_id = v.id')
            ->innerJoin('vvj', 'viajes', 'vj', 'vvj.viaje_id = vj.id');
        $result = $qb2->executeQuery()->fetchAllAssociative();

        $this->data = [
            'status' => true,
            'message' => 'Viaje asignado correctamente',
            'viajeros' => $result
        ];
        return $this->respond($this->data, Response::HTTP_OK);
    }
    /**
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    public function getAllViajerosAssigned(ManagerRegistry $doctrine): JsonResponse
    {
        $qb = new QueryBuilder($doctrine->getConnection());
        $qb->select('*')
            ->from('viaje_viajero', 'vvj')
            ->innerJoin('vvj', 'viajeros', 'v', 'vvj.viajero_id = v.id')
            ->innerJoin('vvj', 'viajes', 'vj', 'vvj.viaje_id = vj.id');
        $result = $qb->executeQuery()->fetchAllAssociative();
          //enq el caso de que no exista ningun registro en la tabla viaje_viajero
        if (!$result) {
            $this->data = [
                'status' => false,
                'message' => 'No hay informacion disponible!',
                'viajeros' => []
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $this->data = [
            'status' => true,
            'viajeros' => $result
        ];
        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param Request $request
     * @param ViajeRepository $viaje
     * @return JsonResponse
     */
    public function createViaje(Request $request, ViajeRepository $viaje): JsonResponse
    {
        /**
         * @var FormInterface $form
         */
        $trip = new Viaje();
        $form = $this->buildForm(ViajeType::class, $trip);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        //si tiene errores que los muestre
        if (!$form->isSubmitted() || !$form->isValid()) {
            $this->data = [
                'status' => false,
                'errors' => $this->thereErrors($form),
            ];
            return $this->respond($this->data, Response::HTTP_BAD_REQUEST);
        }

        /**
         * @var Viaje $newViaje
         */
        $newViaje = $form->getData();
        $viaje->add($newViaje); //guardando el viaje en la base de datos
        $this->data = [
            'status' => true,
            'viaje' => $newViaje->getAllViaje(),
            'message' => 'Viaje creado correctamente',
        ];
        return $this->respond($this->data, Response::HTTP_CREATED);
    }

    /**
     * @param ViajeRepository $viaje
     * @return JsonResponse
     */
    public function allViajes(ViajeRepository $viaje): JsonResponse
    {
        $viajes = $viaje->createQueryBuilder('v')
            ->select('v')
            ->getQuery()
            ->getArrayResult();
        //en el caso que no consiga ningun viajero
        if (!$viajes) {
            $this->data = [
                'status' => false,
                'message' => 'No hay viajes disponibles!',
                'viajes' => []
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $this->data = [
            'status' => true,
            'viajes' => $viajes,
        ];

        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param ViajeRepository $viaje
     * @param Request $request
     * @return JsonResponse
     */
    public function getViaje(Request $request, ViajeRepository $viaje): JsonResponse
    {
        $viajeId = $request->get('viajeId');
        $trip = $viaje->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajeId)
            ->getQuery()
            ->getArrayResult();
        //en el caso que no consiga ningun viajero
        if (!$trip) {
            $this->data = [
                'status' => false,
                'message' => 'El viaje con este ' . $viajeId . ' id no existe!',
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $this->data = [
            'status' => true,
            'viaje' => $trip,
        ];

        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param ViajeRepository $viaje
     * @param Request $request
     * @return JsonResponse
     */
    public function updateViaje(Request $request, ViajeRepository $viaje): JsonResponse
    {
        $viajeId = $request->get('viajeId');

        //consulta del viajero
        $trip = $viaje->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajeId)
            ->getQuery()
            ->getResult();

        //en el caso que no consiga ningun viajero
        if (!$trip) {
            $this->data = [
                'status' => false,
                'message' => 'El viajero con este ' . $viajeId . ' id no existe!',
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        /**
         * @var FormInterface $form
         */
        $form = $this->buildForm(ViajeType::class, $trip[0], [
            'method' => $request->getMethod(),
        ]);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        //si tiene errores que los muestre
        if (!$form->isSubmitted() || !$form->isValid()) {
            $this->data = [
                'status' => false,
                'errors' => $this->thereErrors($form),
            ];
            return $this->respond($this->data, Response::HTTP_BAD_REQUEST);
        }

        /**
         * @var Viaje $updatedViaje
         */
        $updatedViaje = $form->getData();
        $viaje->add($updatedViaje); //guardando el viajero en la base de datos
        $this->data = [
            'status' => true,
            'viaje' => $updatedViaje->getAllViaje(),
            'message' => 'Viaje actualizado correctamente',
        ];
        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param ViajeRepository $viaje
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    public function deleteViaje(Request $request, ViajeRepository $viaje, ManagerRegistry $doctrine): JsonResponse
    {
        $viajeId = $request->get('viajeId');

        $trip = $viaje->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajeId)
            ->getQuery()
            ->getResult();

        //cuando se elimine un viajeri se eliminara lo que esta relaciona con la 
        //tabla viaje_viajero ya que no existe incluyendo con el viaje
        $qb = new QueryBuilder($doctrine->getConnection());
        $qb->delete('viaje_viajero')
            ->where('viaje_viajero.viaje_id = :viajeId')
            ->setParameter('viajeId', $viajeId);
        $qb->executeStatement();

        //en el caso que no consiga ningun viajero
        if (!$trip) {
            $this->data = [
                'status' => false,
                'message' => 'El viaje con este ' . $viajeId . ' id no existe!',
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $viaje->remove($trip[0]);
        $this->data = [
            'status' => true,
            'message' => 'Viaje eliminado correctamente',
        ];
        return $this->respond($this->data, Response::HTTP_OK);
    }
}
