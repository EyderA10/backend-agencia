<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormInterface;
use App\Entity\Viajero;
use App\Form\Type\ViajeroType;
use App\Repository\ViajeroRepository;
use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

// este controlador extendera del contraldor AbstractApi para usar sus metodos
class ViajeroController extends AbstractApiController
{

    /**
     * @var array $data
     */
    private $data;

    public function __construct()
    {
        $this->data = [];
    }
    /**
     * @param Request $request
     * @param ViajeroRepository $viajero
     * @return JsonResponse
     */
    public function createViajero(Request $request, ViajeroRepository $viajero): JsonResponse
    {
        /**
         * @var FormInterface $form
         */
        $viajer = new Viajero();
        $form = $this->buildForm(ViajeroType::class, $viajer);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        //si tiene errores que los muestre
        if (!$form->isSubmitted() || !$form->isValid()) {
            $this->data = [
                'status' => false,
                'errors' => $this->thereErrors($form),
            ];
            return $this->respond($this->data, Response::HTTP_BAD_REQUEST);
        }

        /**
         * @var Viajero $newViajero
         */
        $newViajero = $form->getData();
        $viajero->add($newViajero); //guardando el viajero en la base de datos
        $this->data = [
            'status' => true,
            'viajero' => $newViajero->getAllViajero(),
            'message' => 'Viajero creado correctamente',
        ];
        return $this->respond($this->data, Response::HTTP_CREATED);
    }

    /**
     * @param ViajeroRepository $viajero
     * @return JsonResponse
     */
    public function allViajeros(ViajeroRepository $viajero): JsonResponse
    {
        $viajeros = $viajero->createQueryBuilder('v')
            ->select('v')
            ->getQuery()
            ->getArrayResult();
        //en el caso que no consiga ningun viajero
        if (!$viajeros) {
            $this->data = [
                'status' => false,
                'message' => 'No hay viajeros disponibles!',
                'viajeros' => []
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $this->data = [
            'status' => true,
            'viajeros' => $viajeros,
        ];

        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param ViajeroRepository $viajero
     * @param Request $request
     * @return JsonResponse
     */
    public function getViajero(Request $request, ViajeroRepository $viajero): JsonResponse
    {
        $viajero_id = $request->get('id');
        $viajero = $viajero->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajero_id)
            ->getQuery()
            ->getArrayResult();
        //en el caso que no consiga ningun viajero
        if (!$viajero) {
            $this->data = [
                'status' => false,
                'message' => 'El viajero con este ' . $viajero_id . ' id no existe!',
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $this->data = [
            'status' => true,
            'viajero' => $viajero,
        ];

        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param ViajeroRepository $viajero
     * @param Request $request
     * @return JsonResponse
     */
    public function updateViajero(Request $request, ViajeroRepository $viajero): JsonResponse
    {
        $viajeroId = $request->get('viajeroId');
        //consulta del viajero
        $get_viajero = $viajero->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajeroId)
            ->getQuery()
            ->getResult();

        //en el caso que no consiga ningun viajero
        if (!$get_viajero) {
            $this->data = [
                'status' => false,
                'message' => 'El viajero con este ' . $viajeroId . ' id no existe!',
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        /**
         * @var FormInterface $form
         */
        $form = $this->buildForm(ViajeroType::class, $get_viajero[0], [
            'method' => $request->getMethod(),
        ]);

        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        //si tiene errores que los muestre
        if (!$form->isSubmitted() || !$form->isValid()) {
            $this->data = [
                'status' => false,
                'errors' => $this->thereErrors($form),
            ];
            return $this->respond($this->data, Response::HTTP_BAD_REQUEST);
        }

        /**
         * @var Viajero $updatedViajero
         */
        $updatedViajero = $form->getData();
        $viajero->add($updatedViajero); //guardando el viajero en la base de datos
        $this->data = [
            'status' => true,
            'viajero' => $updatedViajero->getAllViajero(),
            'message' => 'Viajero actualizado correctamente',
        ];
        return $this->respond($this->data, Response::HTTP_OK);
    }

    /**
     * @param ViajeroRepository $viajero
     * @param Request $request
     * @param ManagerRegistry $doctrine
     * @return JsonResponse
     */
    public function deleteViajero(Request $request, ViajeroRepository $viajero, ManagerRegistry $doctrine): JsonResponse
    {
        $viajeroId = $request->get('viajeroId');

        $get_viajero = $viajero->createQueryBuilder('v')
            ->select('v')
            ->where('v.id = :id')
            ->setParameter('id', $viajeroId)
            ->getQuery()
            ->getResult();

        //cuando se elimine un viajeri se eliminara lo que esta relaciona con la 
        //tabla viaje_viajero ya que no existe incluyendo con el viaje
        $qb = new QueryBuilder($doctrine->getConnection());
        $qb->delete('viaje_viajero')
            ->where('viaje_viajero.viajero_id = :viajeroId')
            ->setParameter('viajeroId', $viajeroId);
        $qb->executeStatement();

        //en el caso que no consiga ningun viajero
        if (!$get_viajero) {
            $this->data = [
                'status' => false,
                'message' => 'El viajero con este ' . $viajeroId . ' id no existe!',
            ];
            return $this->respond($this->data, Response::HTTP_NOT_FOUND);
        }

        $viajero->remove($get_viajero[0]);
        $this->data = [
            'status' => true,
            'message' => 'Viajero eliminado correctamente',
        ];
        return $this->respond($this->data, Response::HTTP_OK);
    }
}
