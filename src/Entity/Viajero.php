<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

//Creando el modelo de datos sobre los viajeros

/**
 * @ORM\Table(name="viajeros")
 * @ORM\Entity()
 * @UniqueEntity(
 *     fields={"cedula"},
 *     errorPath="cedula",
 *     message="Esta cedula ya existe en nuestro registro."
 * )
 */
class Viajero
{
    //Creando los campos para la tabla viajeros

    /**
     * @var int
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="cedula", type="string", length=20, unique=true)
     */
    private $cedula;

    /**
     * @var string
     * 
     * @ORM\Column(name="nombre", type="string", length=30)
     */
    private $nombre;

    /**
     * @var \DateTime
     * 
     * @ORM\Column(name="fecha_nacimiento", type="date", length=20)
     */
    private $fecha_nacimiento;

    /**
     * @var string
     * 
     * @ORM\Column(name="num_telf", type="string", length=30)
     */
    private $num_telf;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCedula(): ?string
    {
        return $this->cedula;
    }

    /**
     * @param string $cedula
     */
    public function setCedula(string $cedula): void
    {
        $this->cedula = $cedula;
    }

    /**
     * @return \DateTime|null
     */
    public function getFechaNacimiento(): ?\DateTime
    {
        return $this->fecha_nacimiento;
    }

    /**
     * @param \DateTime $fecha
     */
    public function setFechaNacimiento(\DateTime $fecha): void
    {
        $this->fecha_nacimiento = $fecha;
    }

    /**
     * @return string|null
     */
    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    /**
     * @param string $num
     */
    public function setNombre(string $nombre): void
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string|null
     */
    public function getNumTelf(): ?string
    {
        return $this->num_telf;
    }

    /**
     * @param string $num
     */
    public function setNumtelf(string $num): void
    {
        $this->num_telf = $num;
    }
    /**
     * me retorna un array con los datos que necesito para enviarlos por json
     * 
     * @return array
     */
    public function getAllViajero(): array
    {
        return [
            'id' => $this->getId(),
            'cedula' => $this->getCedula(),
            'nombre' => $this->getNombre(),
            'fecha_nacimiento' => $this->getFechaNacimiento(),
            'num_telf' => $this->getNumTelf()
        ];
    }
}
