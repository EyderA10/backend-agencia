<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

//Creando modelo de datos sobre los Viajes

/**
 * @ORM\Table(name="viajes")
 * @ORM\Entity()
 * @UniqueEntity(
 *     fields={"codigo_viaje"},
 *     errorPath="codigo_viaje",
 *     message="Este codigo de viaje ya existe en nuestro registro."
 * )
 */
class Viaje
{
    //Creando campos para la tabla viajes

    /**
     * @var int
     * 
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * 
     * @ORM\Column(name="codigo_viaje", type="string", length=100, unique=true)
     */
    private $codigo_viaje;

    /**
     * @var int
     * 
     * @ORM\Column(name="numero_plazas", type="integer", length=50)
     */
    private $numero_plazas;

    /**
     * @var string
     * 
     * @ORM\Column(name="destino", type="string", length=50)
     */
    private $destino;

    /**
     * @var string
     * 
     * @ORM\Column(name="lugar_origen", type="string", length=50)
     */
    private $lugar_origen;

    /**
     * @var int
     * 
     * @ORM\Column(name="precio", type="integer", length=10)
     */
    private $precio;

    /**
     * @var Viajero
     * 
     * @ORM\ManyToMany(targetEntity="Viajero")
     */
    private $viajero;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCodigoViaje(): ?string
    {
        return $this->codigo_viaje;
    }

    /**
     * @param string $codigo
     */
    public function setCodigoViaje(string $codigo): void
    {
        $this->codigo_viaje = $codigo;
    }

    /**
     * @return int|null
     */
    public function getNumeroPlazas(): ?int
    {
        return $this->numero_plazas;
    }

    /**
     * @param int $num_pl
     */
    public function setNumeroPlazas(int $num_pl): void
    {
        $this->numero_plazas = $num_pl;
    }

    /**
     * @return string|null
     */
    public function getDestino(): ?string
    {
        return $this->destino;
    }

    /**
     * @param string $destino
     */
    public function setDestino(string $destino): void
    {
        $this->destino = $destino;
    }

    /**
     * @return string|null
     */
    public function getLugarOrigen(): ?string
    {
        return $this->lugar_origen;
    }

    /**
     * @param string $lugar
     */
    public function setLugarOrigen(string $lugar): void
    {
        $this->lugar_origen = $lugar;
    }

    /**
     * @return int|null
     */
    public function getPrecio(): ?int
    {
        return $this->precio;
    }

    /**
     * @param int $precio
     */
    public function setPrecio(int $precio): void
    {
        $this->precio = $precio;
    }

    /**
     *
     * @return Viajero
     */
    public function getViajeros()
    {
        return $this->viajero;
    }

    /**
     * @param Viajero  $viajero
     */
    public function setViajero($viajero): void
    {
        $this->viajero = $viajero;
    }

    /**
     * me retorna un array con los datos que necesito para enviarlos por json
     * 
     * @return array
     */
    public function getAllViaje(): array
    {
        return [
            'id' => $this->getId(),
            'codigo_viaje' => $this->getCodigoViaje(),
            'numero_plazas' => $this->getNumeroPlazas(),
            'destino' => $this->getDestino(),
            'lugar_origen' => $this->getLugarOrigen(),
            'precio' => $this->getPrecio()
        ];
    }
}
