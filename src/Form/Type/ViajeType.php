<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\NotNull;

class ViajeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('codigo_viaje', TextType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar un codigo de viaje',
                    ]),
                ]
            ])
            ->add('numero_plazas', NumberType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar numero de plazas',
                    ]),
                ],
                'invalid_message' => 'El numero de plazas debe ser numerico',
            ])
            ->add('destino', TextType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar un destino',
                    ]),
                ]
            ])
            ->add('lugar_origen', TextType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar un destino',
                    ]),
                ]
            ])
            ->add('precio', NumberType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar un destino',
                    ]),
                ],
                'invalid_message' => 'El precio debe ser numerico',
            ]);
    }
}
