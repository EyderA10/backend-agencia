<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ViajeroType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('cedula', NumberType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar una cedula'
                    ])
                ]
            ])
            ->add('nombre', TextType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar un nombre'
                    ]),
                ]
            ])
            ->add('fecha_nacimiento', DateType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar una fecha de nacimiento'
                    ])
                ],
                'invalid_message' => 'La fecha de nacimiento no es valida',
                'widget' => 'single_text'
            ])
            ->add('num_telf', NumberType::class, [
                'constraints' => [
                    new NotNull([
                        'message' => 'Debe ingresar un telefono'
                    ]),
                ]
            ]);
    }
}
